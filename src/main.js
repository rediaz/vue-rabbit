import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

// 引入初始化的样式文件
import '@/styles/common.scss'
// 引入懒加载插件并且注册（懒加载插件在directives/index.js文件中）
import { LazyPlugin } from '@/directives'

// 引入全局组件插件
import { componentPlugin } from '@/components'
// pinia插件
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const app = createApp(App)
const pinia = createPinia()
// 注册持久化插件
pinia.use(piniaPluginPersistedstate)
app.use(pinia)
app.use(router)
app.use(LazyPlugin)
app.use(componentPlugin)
app.mount('#app')

