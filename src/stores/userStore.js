// 管理用户数据相关
import { loginAPI } from '@/apis/user'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useCartStore } from '@/stores/cartStore'
import { mergeCartAPI } from '@/apis/cart'
export const useUserStore = defineStore("user", () => {
    const cartStore = useCartStore()
    // 1．定义管理用户数据的state
    const userInfo = ref({})
    // 2．定义获取接口数据的action函数
    const getUserInfo = async ({ account, password }) => {
        const res = await loginAPI({ account, password })
        userInfo.value = res.result
        // 合并购物车
        await mergeCartAPI(cartStore.cartList.map(item => {
            return {
                skuId: item.skuId,
                selected: item.selected,
                count: item.count
            }
        }))
        cartStore.updateCartList()
    }
    // 退出用户信息进行清除的逻辑方法
    const clearUserInfo = () => {
        userInfo.value = {}
        // 清除localStorage中的数据
        cartStore.clearCart()
    }

    // 3．以对象的格式把state和action return
    return {
        userInfo,
        getUserInfo,
        clearUserInfo
    }
}, {
    // 运行机制：
    //   在设置state的时候会自动把数据同步给localstorage
    //   在获取state数据的时候会优先从localStorage中取
    persist: true
})