// 封装购物车模块
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useUserStore } from './userStore'
import { insertCartAPI, findNewListAPI, delCartAPI } from '@/apis/cart'

export const useCartStore = defineStore('cart', () => {
    const useStore = useUserStore()
    const isLogin = computed(() => useStore.userInfo.token)
    // 1.定义state-cartList
    const cartList = ref([])
    // 2.定义action-addList
    const addCart = async (goods) => {
        const { skuId, count } = goods
        if (isLogin.value) {
            // 登录之后假如购物车的逻辑
            await insertCartAPI({ skuId, count })
            // 获取最新列表 数据
            const res = await findNewListAPI()
            // 覆盖本地列表
            cartList.value = res.result
        } else {
            // 添加购物车操作 
            // 已添加过-count + 1  
            // 没有添加过–直接push
            // 思路: 通过匹配传递过来的商品对象中的skuId能不能在cartList中找到，找到了就是添加过
            const item = cartList.value.find((item) => goods.skuId === item.skuId)
            if (item) {
                // 找到了
                item.count++
            } else {
                // 没找到
                cartList.value.push(goods)
            }
        }
    }
    // 删除购物车
    const delCart = async (skuId) => {
        if (isLogin.value) {
            // 调用接口中的删除功能
            await delCartAPI([skuId])
            // 获取最新列表数据
            const res = await findNewListAPI()
            // 覆盖本地列表
            cartList.value = res.result
        } else {

            // 思路：1．找到要删除项的下标值- splice  2．使用数组的过滤方法- filter
            const index = cartList.value.findIndex((item) => skuId === item.skuId)
            cartList.value.splice(index, 1)
        }
    }
    // 更新购物车数据
    const updateCartList = async () => {
        const res = await findNewListAPI()
        cartList.value = res.result
    }
    // 清除购物车数据
    const clearCart = () => {
        cartList.value = []
    }

    // 计算属性
    // 1.总的数量 所有项的count之和
    const allCount = computed(() => cartList.value.reduce((a, c) => a + c.count, 0))
    // 2.总价是count*price之和
    const allPrice = computed(() => cartList.value.reduce((a, c) => a + c.count * c.price, 0))
    // 3.计算选中的商品数量
    const selectedCount = computed(() => cartList.value.filter(item => item.selected).reduce((a, c) => a + c.count, 0))
    // 4.计算选中的商品总价格
    const allCheckPrice = computed(() => cartList.value.filter(item => item.selected).reduce((a, c) => a + c.count * c.price, 0))


    // 单选功能
    const singelCheck = (skuId, selected) => {
        // 根据skuId来改变selected的值
        const item = cartList.value.find((item) => item.skuId === skuId)
        item.selected = selected
    }
    // 全选功能
    const allCheck = (selected) => {
        // 修改所有商品的selected值
        cartList.value.forEach(item => item.selected = selected)
    }

    // 购物车是否全选
    const isAll = computed(() => cartList.value.every((item) => item.selected))


    return {
        cartList,
        allCount,
        allPrice,
        isAll,
        selectedCount,
        allCheckPrice,
        addCart,
        delCart,
        singelCheck,
        allCheck,
        clearCart,
        updateCartList
    }
}, {
    // 运行机制：
    //   在设置state的时候会自动把数据同步给localstorage
    //   在获取state数据的时候会优先从localStorage中取
    persist: true
})