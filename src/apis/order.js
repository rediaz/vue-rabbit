import httpInstance from "@/utils/http";

/**
 * 基础列表渲染
 * @param {*} params 
 * @returns 
 */
export const getUserOrder = (params) => {
    return httpInstance({
        url: '/member/order',
        method: 'GET',
        params
    })
}