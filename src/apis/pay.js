import httpInctance from "@/utils/http";


export const getOrderAPI = (id) => {
    return httpInctance({
        url: `/member/order/${id}`
    })
}