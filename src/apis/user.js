// 封装所有与user相关的API
import request from "@/utils/http";



export function loginAPI({ account, password }) {
    return request({
        url: '/login',
        method: 'post',
        data: {
            account, password
        }
    })
}

/**
 * 会员中心猜你喜欢的接口
 * @param {*} param0 
 * @returns 
 */
export const getLikeListAPI = ({ limit = 4 }) => {
    return request({
        url: '/goods/relevant',
        params: {
            limit
        }
    })
}