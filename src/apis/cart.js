// 封装购物车相关接口
import request from "@/utils/http";


/**
 * 加入购物车
 * @param {*} param0 
 * @returns 
 */
export const insertCartAPI = ({ skuId, count }) => {
    return request({
        url: '/member/cart',
        method: 'POST',
        data: {
            skuId,
            count
        }
    })
}


/**
 * 获取最新购物车情况
 * @returns 
 */
export const findNewListAPI = () => {
    return request({
        url: '/member/cart'
    })
}





/**
 * 删除购物车
 * @param {*} ids 
 * @returns 
 */
export const delCartAPI = (ids) => {
    return request({
        url: '/member/cart',
        method: 'DELETE',
        data: {
            ids
        }
    })
}
/**
 * 合并购物车
 * @param {*} data 
 * @returns 
 */
export const mergeCartAPI = (data) => {
    return request({
        url: '/member/cart/merge',
        method: 'POST',
        data
    })
}
