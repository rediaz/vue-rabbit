import { getCategoryAPI } from "@/apis/category";
import { onMounted, ref } from "vue";
import { useRoute, onBeforeRouteUpdate } from "vue-router";

// 封装分类相关业务代码
export function useCategory() {
    const categoryData = ref({});
    const route = useRoute()
    // id = route.params.id，表示有参数的时候，就使用参数id，没有参数的时候，就使用默认值route.params.id
    const getCategory = async (id = route.params.id) => {
        const res = await getCategoryAPI(id);
        categoryData.value = res.result;
    }
    onMounted(() => getCategory());
    // 希望路由参数变化的时候 可以把分类接口重新发送
    onBeforeRouteUpdate((to) => {
        // console.log('路由变化了');
        // 问题：路由参数存在滞后性，所以需要重新传入参数（to：目标路由对象）
        // console.log(to);
        getCategory(to.params.id)

    })
    return {
        categoryData
    }
}