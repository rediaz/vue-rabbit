import { onMounted, ref } from "vue";
import { getBannerAPI } from '@/apis/home';
// 封装轮播图相关业务代码
export function useBanner() {
    const bannerList = ref([])
    onMounted(() => {
        getBannerAPI({
            distributionSite: '2'
        }).then(res => {
            // console.log(res);
            bannerList.value = res.result
        })
    });
    return {
        bannerList
    }
}