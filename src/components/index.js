import ImageVue from './ImageVue/index.vue'
import Sku from './XtxSku/index.vue'
// 把components中的所组件都进行全局化注册
// 通过插件的方式

export const componentPlugin = {
    install(app) {
        // app.component('组件名',组件的配置对象)
        app.component('XtxImageVue', ImageVue)
        app.component('XtxSku', Sku)

    }
}