// 对基础的axios进行封装
import axios from "axios";
// 获取pinia实例
import { useUserStore } from '@/stores/userStore';
import router from '@/router'
// 提示框elementplus
import { ElMessage } from 'element-plus'
import 'element-plus/theme-chalk/el-message.css'
// 不同的业务可以创建不同的http基地址
const httpInctance = axios.create({
    //设置项目的基地址
    baseURL: 'http://pcapi-xiaotuxian-front-devtest.itheima.net',
    timeout: 5000//超时时间
})


// axios请求拦截器 
httpInctance.interceptors.request.use(config => {
    // 1.从pinia中获取token
    const useStore = useUserStore()
    const token = useStore.userInfo.token
    // 2.按照后端要求拼接token
    if (token) {
        config.headers.Authorization = `Bearer ${token}`
    }
    return config
}, e => Promise.reject(e))

// axios响应式拦截器
httpInctance.interceptors.response.use(res => res.data, e => {
    // 统一错误提示
    ElMessage({
        type: 'warning',
        message: e.response.data.message
    })
    // 401token失效处理
    if (e.response.status === 401) {
        const userStore = useUserStore()
        // 1.清除用户信息 触发action
        userStore.clearUserInfo()
        // 2.跳转到登录页
        router.push('/login')
    }
    return Promise.reject(e)
})


export default httpInctance;